
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Código  triangulo. 
"""

l1 = int(input("Lado 1: "))
l2 = int(input("Lado 2: "))
l3 = int(input("Lado 3: "))

if l1 >= (l2 + l3) or l2 >= (l1 + l3) or l3 >= (l1 + l2):
	print ("Não pode ser um triângulo")
elif l1 == l2 == l3:
	print ("Equilátero")
elif l1 == l2 or l1 == l3 or l2 == l3:
	print ("Isósceles")
else:
	print ("Escaleno")
if l1 == 0 and l2 == 0 and l3 == 0:
	print("todos os lado não podem ser zero!")
elif l1 == 0 or l2 == 0 or l3 == 0:
	print("Algum lado é zero!")