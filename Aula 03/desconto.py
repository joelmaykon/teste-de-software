
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Código  desconto.
"""

def desconto(Cliente, Qtd):
    if Cliente == 'A':
        if Qtd >= 1 and Qtd <= 1000:  
            if Qtd < 10:
                print("O Cliente não terá desconto por Qtd igual a 10")
            if Qtd > 10 and Qtd < 99:
                print("O Cliente receberá um desconto de 5%")
            if Qtd >= 100:
                print("O Cliente receberá um desconto de 10%")
        elif Qtd < 1:
            print("Valor abaixo de 1")
        elif Qtd > 1000:
            print("Valor acima de 1000")
    elif Cliente == 'B':
        if Qtd >= 1 and Qtd <= 1000:  
            if Qtd < 10:
                print("O Cliente receberá um desconto de 5%")
            if Qtd > 10 and Qtd < 99:
                print("O Cliente receberá um desconto de 15%")
            if Qtd >= 100:
                print("O Cliente receberá um desconto de 25%")
        elif Qtd < 1:
            print("Valor abaixo de 1")
        elif Qtd > 1000:
            print("Valor acima de 1000")
    elif Cliente == 'C':
        if Qtd >= 1 and Qtd <= 1000:  
            if Qtd < 10:
                print("O Cliente não receberá o desconto")
            if Qtd > 10 and Qtd < 99:
                print("O Cliente receberá um desconto de 20%")
            if Qtd >= 100:
                print("O Cliente receberá um desconto de 25%")
        elif Qtd < 1:
            print("Valor abaixo de 1")
        elif Qtd > 1000:
            print("Valor acima de 1000")

print("______________________________________")
print("Test 01 Cliente A com valor mínimo")
desconto('A', 1)
print("______________________________________")
print("Test 02 Cliente A com valor máximo")
desconto('A', 1000)
print("______________________________________")
print("Test 03 Cliente A com valor abaixo do mínimo")
desconto('A', -1)
print("______________________________________")
print("Test 04 Cliente A com valor acima do máximo")
desconto('A', 1001)
print("______________________________________")
print("Test 05 Cliente A com valor acima de 10")
desconto('A', 11)
print("______________________________________")
print("Test 06 Cliente A com valor acima de 99")
desconto('A', 101)
print("______________________________________")
print("Test 07 Cliente A com valor máximo")
desconto('B', 1)
print("______________________________________")
print("Test 08 Cliente A com valor máximo")
desconto('B', 11)
print("______________________________________")
print("Test 09 Cliente A com valor máximo")
desconto('B', 101)
print("______________________________________")
print("Test 07 Cliente A com valor máximo")
desconto('C', 1)
print("______________________________________")
print("Test 08 Cliente A com valor máximo")
desconto('C', 11)
print("______________________________________")
print("Test 09 Cliente A com valor máximo")
desconto('C', 101)
