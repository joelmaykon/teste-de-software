
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Código contatos de uma agenda.
"""

def contato(Nome, Telefone, email):
    Contato = {"Nome":Nome,"Telefone":Telefone,"email":email}     
    return Contato

def agenda(contatos):   
    contato = []
    agenda = [] 
    email = []
    for item in contatos:
        
        if item['Telefone'] == '':
            print('____________________________________')
            print("Erro ao cadastrar")
            print("Contato: ", item)
            print("Não é possivel registrar na agenda contato sem numero de telefone!")
        elif len(item['Telefone']) < 8:
            print('____________________________________')
            print("Erro ao cadastrar")
            print("Contato: ", item)
            print("Numero precisa ser maior ou igual a 8 digitos")
        elif len(item['Telefone']) > 15:
            print('____________________________________')
            print("Erro ao cadastrar")
            print("Contato: ", item)
            print("Numero precisa ser menor ou igual a 15 digitos")        
        elif item['Telefone'] in contato:
            print('____________________________________')
            print("Erro ao cadastrar")
            print("Contato: ", item)
            print("Numero ",item['Telefone'],"  ja existente na Agenda!")
        elif item['Telefone'] not in contato:
            print('____________________________________')
            contato.append(item['Telefone'])
            if  item['email'] not in  email:     
                arroba = item['email'].find("@")
                ponto = item['email'].rfind(".")
                if arroba < 1:
                    print("Erro ao cadastrar")
                    print("Contato: ", item)
                    print("email não possui @")
                elif ponto < arroba + 2:
                    print("Erro ao cadastrar")
                    print("Contato: ", item)
                    print("não possui ponto apos o arroba")
                else:
                    print("Contato: ", item)
                    print("email valido")
                    agenda.append(item)
        
    print('____________________________________')
    print("Contatos cadastrados")
    for item in agenda:        
        print("Contato: ", item)
    return agenda
        
contato_1 = contato("Joel",'99520403',"joel@email.com")
#Teste a) valor invalido: sem numero de telefone
contato_2 = contato("Leandro","","leandro@email.com")
#Teste b) valor invalido: Repetição de um numero ja existente
contato_3 = contato("Larissa",'99520403',"larissa@email.com")
#Teste c) valor invalido: numero menor que 8
contato_4 = contato("Samara",'9520403',"samara@email.com")
#Teste c) valor invalido: numero maior que 15
contato_5 = contato("Samara",'9999952040323212',"samara@email.com")
# valor invalido: email sem o  @ e com ponto '.' depois do @
contato_6 = contato("Samara",'99520405',"samara_email.com")
# valor invalido: email com @ mas sem ponto depois
contato_7 = contato("Samara",'99520409',"samara@emailcom")
# valor invalido: email sem o ponto apos o @ e com ponto antes
contato_8 = contato("Samara",'99520419',"samara.xavier@emailcom")
# valor valido com @ e ponto apos o arroba
contato_9 = contato("Samara",'99520420',"samara.xavier@email.com")


inserir_contatos = [contato_1,contato_2,contato_3,contato_4, contato_5,contato_6,contato_7,contato_8]
agenda(inserir_contatos)
