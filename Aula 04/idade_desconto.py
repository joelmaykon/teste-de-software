
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Código  idade desconto.
"""

def desconto(idade):
    if idade > 0 and idade < 24:
        if idade <= 12:
            print('desconto de 15%')
        elif idade >= 13 and idade <= 18:
            print("desconto de 12%")
        elif idade >= 19 and idade <= 21:
            print("desconto de 5%")
        elif idade >= 22 and idade <= 24:
            print("desconto de 3%")
    elif idade == 0:
        print("idade nao pode ser 0")
    elif idade < 0:
        print("idade nao pode ser menor que zero")
# testando zero
desconto(0)
#testando o menor valor valido
desconto(1)
# testando o maior valor valido
desconto(24)
# testando valor abaixo de zero
desconto(-1)
# testando valor acima do permitido
desconto(25)
# idade ate 12 anos
desconto(12)
# idade maior que 12 e menor que 19
desconto(13)
# idade maior que 18 e menor que 21
desconto(19)
#idade maior que 21 e menor que 24
desconto(22)

