
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Código  triangulo.
"""

def triangulo(l1, l2, l3):

    if l1 > 0 and l2 > 0  and l3 > 0 and l1 < 1000 and l2 < 1000 and l3 < 1000:
        if l1 < l2 + l3 and l2 < l1 + l3 and l3 < l2 + l1:
            if l1 == l2 == l3:
                print("É equilátero")
            elif l1 == l2 or l1 == l3 or l2 == l3:        
                    print("É Isosceles!")    
            elif l1 != l2 and l2 != l3 and l1 != l3:
                print("É escaleno")            
        else: 
            print("Não é um triangulo")
    elif  l1 > 1000 or l2 > 1000 or l3 > 1000:
        print("Lado supera o limite de 1000")
    else:
        print("Lado do triangulo não pode ser zero ou menor que zero!")
print("______________________________________")
print("Test 01 equilatero")
triangulo(20, 20, 20)
print("______________________________________")
print("Test 02 Isosceles")
triangulo(20, 30, 20)
print("______________________________________")
print("Test 03 escaleno")
triangulo(20,30,40)
print("______________________________________")
print("Test 04 lado maior que os outros dois")
triangulo(50,20,20)
print("______________________________________")
print("Test 05 lado igual a zero")
triangulo(0,20,20)
print("______________________________________")
print("Test 06 lado igual a menor que zero")
triangulo(-1,20,20)
print("______________________________________")
print("Test 07 lado maior que 1000")
triangulo(1001,20,30)



